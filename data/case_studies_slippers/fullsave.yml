title: FullSave
file_name: fullsave
canonical_path: /customers/fullsave/
twitter_image: /images/blogimages/fullsave-social-image-1800x945-1.png
cover_image: /images/blogimages/fullsave-banner.jpeg
cover_title: French telecom FullSave uses GitLab to reduce DevOps toolchain and
  dramatically multiply deployments
cover_description: A change in the licensing model of the company's previous
  issue tracking and project management tool, plus a desire to consolidate their
  toolchain, pushed FullSave to switch to GitLab.
customer_logo: /images/case_study_logos/fullsave-logo-sans-baseline.png
customer_industry: Telecommunications
customer_location: Labège, France
customer_solution: GitLab Ultimate SaaS
customer_employees: "100"
customer_overview: FullSave, a telecommunications infrastructure operator based
  in France, is able to release software faster and more efficiently with
  GitLab's DevSecOps Platform.
customer_challenge: FullSave, a telecommunications infrastructure operator based
  in France, is able to release software faster and more efficiently with
  GitLab's DevSecOps Platform.
key_benefits: |-
  Eliminated an inefficient toolchain

  Increased code output

  Automated project deployment
customer_stats:
  - stat: 95%
    label: decrease in deployment time
  - stat: 12x
    label: increase in deployment frequency
customer_study_content:
  - subtitle: The customer
    content: >-
      [FullSave](https://www.fullsave.com/) is a telecommunications
      infrastructure operator based in Labège, France. Founded in 2004, the
      private company has about 100 employees and provides connectivity
      services, cloud infrastructure, and shared hosting in its data centers. It
      also deploys and operates its own fiber network and offers adapted
      internet access services.


      FullSave has a history with GitLab. While the company has used several other DevOps tools for more than five years, it has also been using different versions of GitLab’s platform, such as the free version and the Enterprise edition. For instance, seven developers used the free version to deploy 302 projects, along with managing about 100 issues and 50 merge requests per month. Other teams used GitLab to exchange source code and configuration files with customers, to build and launch network and data center tools, and to automate project deployments and Docker builds.
  - subtitle: The challenge
    content: >-
      FullSave faced several challenges that were causing development and
      deployment slowdowns.


      The company’s policy is to host its own tools but its previous tool’s new licensing model did not allow for self-hosting. As FullSave was met with this licensing issue, the company’s IT managers also recognized that they needed to replace their DevOps toolchain with a single, end-to-end DevOps platform for better CI/CD integration, to reduce complexity and boost productivity. They also knew that a single application would give them the issue and commit traceability they needed to obtain an ISO27001 certification, an internationally recognised standard that deals with the overall management of information security. 


      Going all in with GitLab’s platform was an easy decision for FullSave.


      “We’ve been using GitLab for several years as it had all the features we needed,” says Laurent Lavallade, chief technology officer at FullSave. “This helped us consolidate from the use of several tools to a single platform that had all the features integrated within it. Switching to GitLab’s Enterprise edition was the natural next step.”


      GitLab checked all of their boxes. And FullSave’s IT professionals were familiar with everything the platform brings to the table – increased collaboration, efficiency, security, and automation. 


      In their last environment, integration between FullSave’s previous tool and GitLab did work but it did not work as well, or as efficiently, as simply using GitLab’s single, end-to-end platform alone.
  - subtitle: The solution
    content: >-
      By replacing FullSave’s old issue tracking and project management tool and
      upgrading to GitLab Ultimate SaaS, FullSave has been able to take on its
      biggest DevOps challenges.


      For instance, developers previously were merging issues directly into the development branch. But thanks to GitLab's merge request workflow, FullSave has been able to solve challenges around the validation of code changes and increase efficiency. 


      Collaboration across teams also has improved with GitLab. Issue dependencies, for example, help front-end and back-end teams see where a project stands, know when they need one another, and communicate more readily and easily. That kind of collaboration helps team members share responsibilities and reduces individual efforts. It also increases everyone’s overview of a project and the progress it’s making along its lifecycle.
  - subtitle: The results
    content: >-
      By more fully adopting GitLab’s single platform, FullSave has been able to
      improve communication, collaboration, and efficiencies in both development
      and deployment. All of this eased their entire software development
      lifecycle.


      One of the biggest improvements FullSave’s team has seen is in the speed of development and deployment. Before expanding their use of GitLab, they generally saw deployments only two or three times a month. Now, they are deploying software many times a day, and those deployments are a lot cleaner. Previously, many deployments were done manually, but now with GitLab’s deployment automation, there has been a noticeable reduction in errors and deployment time has been slashed from two to three hours to a few minutes.


      Increased use of GitLab is:


      * Decreasing integration issues and errors

      * Improving software quality

      * Increasing code output

      * Simplifying software development processes and workflow

      * Enabling FullSave to obtain their ISO27001 certification, which some of its customers require them to have

      * Facilitating collaboration

      * Helping staff plan and build roadmaps and custom boards


      The company has experienced a lot of benefits to handling so many parts of the software lifecycle within a single platform, instead of relying on a complex toolchain. GitLab’s all-in-one solution empowers FullSave’s developers and gives them clarity over projects and how they are progressing, so team members can see where they stand and how they can contribute.


      And by enabling collaboration and making developers’ work more efficient, team members now have additional time to create more, and better software products with increased security. For a small team like the one at FullSave, that means they are able to maintain more projects because they use GitLab.
customer_study_quotes:
  - blockquote: GitLab is an all-in-one solution that offers clarity and helps to
      improve the whole team’s efficiency.
    attribution: Laurent Lavallade
    attribution_title: Chief Technology Officer, FullSave
