---
layout: handbook-page-toc
title: Support Team APAC
description: Support Team APAC home page
---

## Welcome to Support Team APAC's Handbook page!

This page documents items specific to Support Team APAC which we have not yet
found a home for in the wider Support Team Handbook.

The intent is to enable APAC Support team members to contribute to Results for
APAC-specific iniatitives, policies, processes and workflows by prioritizing:

1. Transparency, through being [handbook first](https://about.gitlab.com/handbook/handbook-usage/#why-handbook-first)
   and providing a single source of truth for APAC-specific.
1. Iteration, through providing a safe space where APAC Support team members can
   introduce or update APAC-specific policies, workflows and processes without
   creating confusion and uncertainty for the wider Support Team.

Where appropriate, we should always look to move content from this page into
other pages of the wider Support Team Handbook. For an example of how this can
be done, see the [Considerations in APAC section](https://about.gitlab.com/handbook/support/on-call/#considerations-in-apac)
of the GitLab Support On-Call Guide Handbook page.

## General policies

### Support Team APAC is One Team

* APAC Support Readiness department members are part of Support Team APAC too.
* Avoid calling individual manager's direct reports group "my team" or "your team".

### Support engineers should be able to work across all GitLab product platforms

* Support engineers should be willing and able to work on problems for all
  platforms supported by GitLab: SaaS, Dedicated and Self-Managed.

### Support engineers should spend time on work other than L&R work

* ???

## FY24-Q1 Motto: Solve tickets faster

We are evaluating turning this into our north star for FY24. Please leave any
thoughts or feedback in the [discussion issue](https://gitlab.com/gitlab-com/support/support-team-meta/-/issues/4921).
