---
layout: markdown_page
title: "FedRAMP Execution Working Group"
description: "The charter of this working group is to drive execution of FedRAMP compliance."
canonical_path: "/company/team/structure/working-groups/fedramp-execution/"
---

## On this page
{:.no_toc}

- TOC
{:toc}

## Attributes

| Property        | Value             |
|-----------------|-------------------|
| Date Created    | November 25, 2021 |
| End Date        | TBD               |
| Slack           | [#wg_fedramp](https://gitlab.slack.com/archives/C0110E0NMT9) (only accessible from within the company) |
| Google Doc      | [Working Group Agenda](https://docs.google.com/document/d/1icu5A6xPcU-VupOu2TtSCxoCwhMaN0ZsxZVvFfUnj70/edit) (only accessible from within the company) |
| Epic            | [Main Project Epic](https://gitlab.com/groups/gitlab-org/-/epics/8455) (only accessible from within the company) |
| Epic            | [Top Initiative Epic](https://gitlab.com/groups/gitlab-com-top-initiatives/-/epics/3) ([limited access](https://about.gitlab.com/handbook/communication/confidentiality-levels/#limited-access)) |

### Exit Criteria

This working group will organize all the domain experts needed, surface critical decisions, centralize status, and drive execution. More specific exit criteria will be developed early in the process.

## Roles and Responsibilities

| Working Group Role             | Team Member     | Functional Title                           |
|--------------------------------|-----------------|--------------------------------------------|
| Facilitator                    | Julia Lake      | Sr. Director, Security Assurance           |
| Functional Lead: PM            | Chris Balane    | Sr. Product Manager, US Public Sector Services |
| Functional Lead: Pub Sec       | Bob Stevens     | Area Vice President, Public Sector Sales   |
| Functional Lead: Infrastructure | Stephen Dumesnil | Engineering Manager, US Public Sector Services |
| Functional Lead: Compliance    | Corey Oas       | Manager, Security Compliance, Dedicated Markets |
| Executive Stakeholder          | David DeSanto   | Chief Product Officer                      |
| Member                         | Chris Maurer    | Public Sector Manager, Customer Success    |
| Member                         | Vincy Wilson    | Quality Engineering Manager, Fulfillment, Growth, Sec, Enablement |
| Member                         | Joanna Shih     | Quality Engineering Manager, Ops           |

## Direction

GitLab is pursuing FedRAMP authorization at the [Moderate Impact Level](https://www.fedramp.gov/understanding-baselines-and-impact-levels/#moderate-impact-level).
We will evaluate demand for High once we attain Moderate.

Further details of the FedRAMP plan, including anticipated or actual dates, cannot be shared publicly at this time.
Team members can view details [in the internal handbook](https://internal-handbook.gitlab.io/handbook/engineering/fedramp-compliance/).
