---
layout: markdown_page
title: "GitLab Administration Working Group"
description: "Learn more about the GitLab Administration Working Group attributes, goals, roles and responsibilities."
canonical_path: "/company/team/structure/working-groups/administration/"
---

## On this page
{:.no_toc}

- TOC
{:toc}

## Attributes

| Property       | Value                                                        |
| -------------- | ------------------------------------------------------------ |
| Date Created   | 2023-03-01                                           |
| End Date       | TBD                                                          |
| Slack          | #wg_administration (only accessible from within the company) |
| Google Doc     | [Working Group Agenda](https://docs.google.com/document/d/1WKxclMpCIXzXJUcQ0WfsjT_x8ytXuw40KHCoR2LUMzk/edit# )|
| Issue Board    | TBD |
| Epic           | TBD |
| Overview & Status | TBD |

### Charter

The GitLab Administration Working Group will determine the work needed to maintain [GitLab Administrator](https://docs.gitlab.com/ee/administration/) functions healthy, identify DRI groups for requests, and redirect issues to corresponding groups for resolution. This group is a temporary DRI for all GitLab Administrator functions until a team is funded to be a permanent DRI.

### Context

The maintenance and enhancement of GitLab Administration functions has been a shared responsibility among Development groups. This often triggers the same questions about shared responsibilities - who owns this and who should work on this. Before a development DRI group can be identified, a working group will help us to maintain the related functions to run GitLab (the software) smoothly.

### Exit Criteria 

1. DRIs of the items in [this spreadsheet](https://docs.google.com/spreadsheets/d/1Qz8BCuop-M_s6xF5CEpdHWAjgPAY8NAX3PHLzm90m_4/edit#gid=0) are identified, acknowledged, and documented in the handbook.
2. DRIs of things related to instance management (upgrade, backup and restore, metrics, performance) and configuration are identified, acknowledged, and documented in the handbook.
3. A proposal for a new team in case there is a need for certain items of the GitLab Administrator features. There is a chance this is unnecessary if the above two items exhaust all features.
4. A process of identifying DRIs for future administration features is documented in the handbook.

## Roles and Responsibilities

| Working Group Role                       | Person                           | Title                                                           |
|------------------------------------------|----------------------------------|-----------------------------------------------------------------|
| Executive Stakeholder                    |                                  |                                                                 |
| Facilitator/DRI                          |                                  Peter Lu (@plu8) |                                                               Engineering Manager, Distribution:Deploy |
| Functional Lead                          |                                  |                                                                 |
| Product Management DRI                   | Dilan Orrino                     |  Product Manager, Distribution                                  |
| Member                                   | Gosia Ksionek (@mksionek)        |  Engineering Manager, Manage::Organization                      |
| Member                                   | Christina Lohr (@lohrc)        |  Product Manager, Manage::Organization                      |
| Member                                   | Sean Carroll (@sean_carroll)     | Engineering Manager, Create::Source Code                        |
| Member                                   | Joseph Longo (@jlongo_gitlab)  | Manager, Governance and Field Security |
| Member                                   | Oriol Lluch Parellada (@o-lluch)  | Engineering Manager, Infrastructure |
| Member                                   | Hannah Sutor (@hsutor)  | Senior Product Manager - Auth |
| Member                                   | Falko Sieverding (@fsieverding)  | Customer Success Manager, EMEA |
