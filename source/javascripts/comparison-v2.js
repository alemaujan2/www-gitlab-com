$(function () {
  const maxScore = 5
  const minScore = 0
  const greenScore = 3.75
  const orangeScore = 2
  const scoreToWidthRatio = 100 / maxScore

  const _toggleCategoryView = function () {
    $(this).parent().toggleClass('use-case-expanded').toggleClass('use-case-collapsed')
  }
  const _toggleSubcategoryView = function () {
    $(this).parent().toggleClass('sub-use-case-expanded').toggleClass('sub-use-case-collapsed')
  }
  const _fillScoreBox = function () {
    const score = $(this).attr('score');

    const _getBar = function (score, cssClass, content = '') {
      const width = score * scoreToWidthRatio
      return `<div class='${cssClass}' style='width: ${width}%;'> ${content} </div>`;
    }

    const _getLeftBar = function (score, content = '') {
      let color = 'red-bar'
      if (score >= greenScore) {
        color = 'green-bar'
      }
      else if (score >= orangeScore) {
        color = 'orange-bar'
      }

      const cssClass = `${color} text left-rounded ${(score == maxScore ? 'right-rounded' : 'right-flat')}`
      return score > minScore ? _getBar(score, cssClass, content) : ''
    }

    const _getRightBar = function (score, content = '') {
      const color = score > minScore ? 'gray-bar' : 'not-supported-bar'
      const cssClass = `${color} right-rounded ${score > minScore ? 'left-flat' : 'left-rounded'}`
      return score < maxScore ? _getBar(maxScore - score, cssClass, content) : ''
    }

    $(this).removeAttr('score');

    const content = $(this).html()
    $(this).html(
      _getLeftBar(score, content) + _getRightBar(score)
    )
  }

  $('.tr.use-case-row .td:first-child').on('click', _toggleCategoryView);
  $('.tr.sub-use-case-row .td:first-child').on('click', _toggleSubcategoryView);

  $('.score-box').each(_fillScoreBox)

  _getTableScrollPosition = function (event) {
    const compareTable = $('.compare-table')
    if (event.target.scrollTop == 0) {
      compareTable.removeClass('scroll')
    }
    else if (!compareTable.hasClass('scroll')) {
      compareTable.addClass('scroll')
    }
  }

  $('.compare-table .tbody').on("scroll", _getTableScrollPosition)
});
