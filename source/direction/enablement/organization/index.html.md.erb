---
layout: markdown_page
title: "Group Direction - Organization"
description: " "
canonical_path: "/direction/enablement/organization/"
---

- TOC
{:toc}

## The Organization group

**Content last reviewed on 2023-02-21**

This is the direction page for the Organization group which is part of the [Enablement section](/direction/enablement/) of the DevOps lifecycle and is responsible for the following categories: 

|  Category   |   Direction  |  Description | Maturity  |
|  ---   |   ---   |   ---   |  ---  |
| Subgroups | [Direction Page](https://about.gitlab.com/direction/enablement/subgroups/)| Groups represent collections of users or projects.| [Complete](https://about.gitlab.com/direction/maturity/)|
| Projects | |Projects are containers of resources used to host your codebase, track issues, plan work, and continuously build, test, and use built-in CI/CD to deploy your app.| [Not Applicable](https://about.gitlab.com/direction/maturity/)|
| User Profile |[Directon Page](https://about.gitlab.com/direction/enablement/user-profile/)|Users represent individuals using GitLab.| [Not Applicable](https://about.gitlab.com/direction/maturity/) |


## Introduction and how you can help
Thanks for visiting this direction page on Organizations in GitLab. This vision and direction is constantly evolving and everyone can contribute:
* Please comment in the linked issues and epics on this page. Sharing your feedback directly on GitLab.com or submitting a merge request to this page are the best ways to contribute to our strategy.
* Please share feedback directly via email, or [schedule a video call](https://calendly.com/christinalohr/30min). If you're a GitLab user and have direct feedback about your needs for the organization, we'd love to hear from you.

### Overview

The Organization group focuses on creating a better experience for organizations to manage their GitLab experience. This includes making administrative capabilities that previously only existed for self-managed users available to our SaaS users as well, such as management of users and groups and supporting cascading settings. This also introduces a new concept for feature parity between projects, subgroups and groups. The result of this effort will be a more intuitive user experience for all our users towards more aligned behaviors throughout our product.

### Vision

The video below contains a discussion with Sid on this topic outlining the problem and the concept of an "instance group". We have since moved away from the term "instance group" and are instead using the term "organization".

<figure class="video_container">
  <iframe src="https://www.youtube.com/embed/U8wTrxM35ow" frameborder="0" allowfullscreen="true"> </iframe>
</figure>

The video below gives a walkthrough of our MVC plan for Organization.

<figure class="video_container">
  <iframe src="https://www.youtube.com/embed/BJ9XYpbzuoI" frameborder="0" allowfullscreen="true"> </iframe>
</figure>

Today, GitLab's features exist at 3 levels:

| Level | Description | Example |
| ------ | ------ | ------ |
| Instance | Features for the entire instance. These are generally admin features (restricted to the admin panel or via a config like `gitlab.rb`), but not always ([Operations Dashboard](https://docs.gitlab.com/ee/user/operations_dashboard/)). | [LDAP](https://docs.gitlab.com/ee/administration/auth/ldap/#configuration-core-only), [admin-level push rules](https://docs.gitlab.com/ee/user/admin_area/#admin-area-sections) |
| Group | Features configured and used at the group-level. These generally inherit behavior or objects down into subgroups (like epics, settings, or memberships). | [Epics](https://docs.gitlab.com/ee/user/group/epics/) |
| Project | Features used at the project level. | [Requirements Management](https://docs.gitlab.com/ee/user/project/requirements/) |

This leads to a few problems:
* 3 ways to build a feature lead to follow-on requests to build a feature at some other level (see meta issues like [group level things](https://gitlab.com/gitlab-org/gitlab/-/issues/17836)) and additional engineering effort.
* Restricts the audience, especially with instance-level features. Many of these features are admin-only, which is a tiny percentage of most users on an instance. If we make a feature instance level, we're locking ourselves into a few thousand self-managed users and locking out GitLab.com users.
* Poor UX. Inconsistencies between the features available at the project and group levels create navigation and usability issues. Moreover, there isn't a dedicated place for organization-level features, so cool features like the Operations Dashboard and the Security Dashboard get relegated to the "More" dropdown in the top Navigation Bar.

While it would be ideal to have one way to build a new feature, most GitLab functionality should exist at the group level rather than the instance level at a minimum.

We should extract features from the admin panel into a new object called *Organization* that cascades behavior to all the projects and groups that are owned by the same user or organization. 
* An organization includes settings, data, and features from all groups, subgroups, and projects under the same owner (including personal namespaces).
* Data from all groups, subgroups and projects in an organization can be aggregated. 

### Goals

The Organization group focuses on creating a better experience for organizations to manage their GitLab experience. This includes making administrative capabilities that previously only existed for self-managed users available to our SaaS users as well, such as management of users and groups and supporting cascading settings. This also introduces a new concept for feature parity between projects, subgroups and groups. The result of this effort will be a more intuitive user experience for all our users towards more aligned behaviors throughout our product.

#### Near term goals

* [Migrate basic functionality of groups and projects to namespaces](https://gitlab.com/groups/gitlab-org/-/epics/6585) while working with other teams to migrate their features to namespaces.

* Move administrative features to the [group level](https://gitlab.com/groups/gitlab-org/-/epics/7411) for group owners.

#### Mid term goals

* [Consolidate & Cascade settings](https://gitlab.com/groups/gitlab-org/-/epics/4419) - this will allow cascading and enforcement of settings top down in the hierarchy.

* [Implement the top level namespace](https://gitlab.com/groups/gitlab-org/-/epics/4257) - this is similar to the existing top level group, but we will create a new landing page experience for organizations to easily view and action on different aspects of their groups, projects, users, settings and more.

## Jobs to be done

<%= partial("direction/jtbd-list", locals: { stage_key: "Workspace" }) %>

## Terminology 

Namespace is the logical object container in the code. 
Organization is the name for the top-level group.

## What are we working on and why?

Our current focus areas and engineering investment are broken down by category below, percentages represent how much engineering time on average is allocated in each milestone.

#### Projects - 70%

Our current focus is on consolidating groups and projects into one generic namespace container. The highest priority at the moment is to [migrate basic project functionality to namespaces](https://gitlab.com/groups/gitlab-org/-/epics/6585). This will enable us to make project functionality available at the group level.

#### Subgroups - 30%

Building onto the migration efforts described above, we are looking to provide functionality at the group level that was previously only available at the project level. First iterations of this effort will be to make the [archiving](https://gitlab.com/gitlab-org/gitlab/-/issues/382051) and [starring](https://gitlab.com/groups/gitlab-org/-/epics/9298) functionality available at the group level, which are some of the most requested features from our customers. 

Another big pain point that our SaaS users have is the ability to control their users and groups, which exists in the admin panel for self-managed users. In order to overcome this and create feature parity, we will [migrate administrative capabilities](https://gitlab.com/groups/gitlab-org/-/epics/7314) to the group and project level so that group/project owners can have more control over their members.

#### User Profile - 0-5%

To ensure we can make progress in the other categories, we are currently deprioritizing work on the User Profile. We are supporting but not actively working on improvements. 

### Demo of introducing the new hierarchy concept for groups and projects for epics.

<figure class="video_container">
  <iframe src="https://www.youtube.com/embed/fE74lsG_8yM" frameborder="0" allowfullscreen="true"> </iframe>
</figure>

## What is not planned right now

* The Organization team will not be responsible for migrating all features from a group or project to a namespace. We will [build a framework](https://gitlab.com/groups/gitlab-org/-/epics/6473) and document the process for features to be migrated. We'll then collaborate with the respective teams to migrate existing functionality to namespace.
* Since we're focused on merging groups and projects at this time, we don't plan to make significant improvements to them, as this would create redundant work. 
* We are not planning to make improvements to user profiles in the near term since the Organization group's capacity will be dedicated to merging groups and projects.
