---
layout: markdown_page
title: "Category Direction - Source Code Management"
---

- TOC
{:toc}

## Source Code Management

| Stage | [Create](/direction/create/) |
| --- | --- |
| Maturity | [Loveable](/direction/maturity/) |
| Content Last Reviewed | `2023-02-23` |

### Introduction and how you can help
<!-- Introduce yourself and the category. Use this as an opportunity to point users to the right places for contributing and collaborating with you as the PM -->

The Source Code Management direction page belongs to the [Source Code](/handbook/product/categories/#source-code-group) group of the [Create](/stages-devops-lifecycle/create/) stage, and is maintained by [Torsten Linz](https://gitlab.com/tlinz), who can be reached at tlinz@gitlab.com. 

This direction is a work in progress, and everyone can contribute. Sharing your feedback directly is the best way to contribute to our strategy and vision. Please, comment and contribute in the linked issues below, or to any other [issues](https://gitlab.com/groups/gitlab-org/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=group%3A%3Asource%20code) or [epics](https://gitlab.com/groups/gitlab-org/-/epics?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=group%3A%3Asource%20code), or raise an issue yourself in [gitlab-org/gitlab](https://gitlab.com/gitlab-org/gitlab/-/issues/) if you don't find existing feedback that matches your thoughts.  

### Strategy and Themes
<!-- Describe your category. Capture the main problems to be solved in market (themes). Describe how you intend to solve these with GitLab (strategy). Provide enough context that someone unfamiliar with the details of the category can understand what is being discussed. -->
Source code management (SCM) is the foundation of an organization's software development practice.  

Building great software depends on teams working well together. Teams can rarely be divided into areas of complete independence. As cross-functional security, compliance, and growth teams are formed, or new services and libraries are created, effective coordination and collaboration are essential. 

To achieve this, teams need to protect production while making it easy for everyone to contribute. Source Code Management provides the controls to define the rules and workflows for this purpose:  
- Who has the rights to view or change the code ([visibility](https://docs.gitlab.com/ee/user/public_access.html); [members](https://docs.gitlab.com/ee/user/project/members/); [permissions](https://docs.gitlab.com/ee/development/permissions.html); [merge request approvals](https://docs.gitlab.com/ee/user/project/merge_requests/approvals/index.html); [forking and mirroring](https://docs.gitlab.com/ee/user/project/repository/forking_workflow.html)) 
- Who owns the code and is responsible for it and thus has the right to approve changes to it ([codeowners](https://docs.gitlab.com/ee/user/project/code_owners.html); [merge request approvals](https://docs.gitlab.com/ee/user/project/merge_requests/approvals/index.html)) 
- Which quality tests must be passed before changes can be applied ([coverage checks](https://docs.gitlab.com/ee/ci/pipelines/settings.html#coverage-check-approval-rule); [security checks](https://docs.gitlab.com/ee/user/application_security/policies/scan-result-policies.html)) 
- Which branches need to be protected because they influence production ([default branch](https://docs.gitlab.com/ee/user/project/repository/branches/default.html); [protected branches](https://docs.gitlab.com/ee/user/project/protected_branches.html); [push rules](https://docs.gitlab.com/ee/user/project/repository/push_rules.html)) 


#### Themes  
While the principles have been around for half a century, SCM is not without challenges. SCM needs to address a large set of requirements that are partly contradicting:  
- encourage sound development practices  
- but also, be flexible enough to adjust to varying workflows  
- be intuitive   
- but also, be secure and ensure compliance  
- be fast and reliable   
- but easy to manage 

[Git](https://git-scm.com/) is the leading Version Control System (VCS) and is loved by developers. It excels at tracking changes in source code and makes it easy and transparent to merge changes from different developers into one code base. GitLab's source code management builds on top of Git adding functionality that aims to address the above requirements. For example, access control to code repositories or requiring code reviews before merging changes. Source code management and the create stage represent the most popular features in GitLab.  

Yet, despite their appeal, neither Git nor GitLab SCM are perfect. Here are the current main shortcomings:  
- GitLab's SCM UX, has shown to be partly unintuitive or uninformative. For instance, controls to enforce rules are hard to discover and understand.   
- Git is not particularly good at working with binary files such as graphics or video content common in game development. With those media files, Git cannot interpret change on a granular level as it does with text files. So, it needs to store full copies of every new version of a media file.   
- While [Git Large File Storage (LFS)](https://docs.gitlab.com/ee/topics/git/lfs/) aims to address this, the Git process - as helpful as it is for developers - appears complex from a content creator's perspective. Especially as they cannot benefit in the same way from Git as developers do for the lack of diffability of media files.  
- Finally, Git gets slow when repositories become exceptionally large (even if they do not contain binary files). While not common such so-called monorepos are used in several large tech companies. [Partial clone](https://docs.gitlab.com/ee/topics/git/partial_clone.html) addresses some of the issues.  

#### Strategy
Therefore, the vision for Source Code Management at GitLab consists of three pillars:  
- Make it easy and intuitive to manage source code so that it is secure, compliant, and encourages best development practices.    
- Make it easy to work with large binary files in GitLab. And make it easy for content creators to contribute media files to GitLab to enable better collaboration with developers to support [GitLab's mission](https://about.gitlab.com/company/mission/#mission): everyone can contribute!  
- Make it easy to work with [monorepos](https://about.gitlab.com/direction/monorepos/) in GitLab: [&8262](https://gitlab.com/groups/gitlab-org/-/epics/8262). (Note, this also touches other stages, especially the Verify stage). 

Note: SCM is not only the most used function in GitLab but also the one with the longest history as it has been there from the beginning. As a result, we get a lot of feedback and have a long backlog of issues. Therefore, we need to spend a considerable share of our teams’ capacity on issues that are not at the center of this vision but address bugs, stability, security, and scalability to keep our users and customers happy. 


### 1 year plan

<!--1 year plan for what we will be working on linked to up-to-date epics. This section will be most similar to a "road-map". Items in this section should be linked to issues or epics that are up to date. Indicate relative priority of initiatives in this section so that the audience understands the sequence in which you intend to work on them. -->

<figure class="video_container">
  <iframe src="https://www.youtube.com/embed/NFAkZGuLIcE?start=135&end=405" frameborder="0" allowfullscreen="true"> </iframe>
</figure>

- Improve experience with branch protections, approval rules, security approvals and status checks in project settings: [View and edit source code rules organized by branches](https://gitlab.com/groups/gitlab-org/-/epics/8075)
- Improve experience with Codeowners: [Advanced Validation of CODEOWNERS file to user prevent mistakes](https://gitlab.com/groups/gitlab-org/-/epics/9819)

To be extended.

#### What is next for us
<!-- This is a 3 month look ahead for the next iteration that you have planned for the category. This section must provide links to issues or
or to [epics](https://about.gitlab.com/handbook/product/product-processes/#epics-for-a-single-iteration) that are scoped to a single iteration. Please do not link to epics encompass a vision that is a longer horizon and don't lay out an iteration plan. -->

- View source code rules in one single place and organized by branches rather than by type of rule: [MVC: Branch Rules Overview](https://gitlab.com/groups/gitlab-org/-/epics/8578#release-post)
- Improving Forking experience: [Allow users to merge from a forked repository](https://gitlab.com/gitlab-org/gitlab/-/issues/384166)
- More transparency on which branches and tags contain a specific commit: [Better display of tags and branches in commit detail view](https://gitlab.com/gitlab-org/gitlab/-/issues/325871)
- Improve experience when defining and maintaining branch protections, approval rules, security approvals and status: [Enable E2E workflow for setting branch rules - from discovery to changing the rule.](https://gitlab.com/groups/gitlab-org/-/epics/9810)
- Improve experience with CODEOWNERS:
    - [Enhance CODEOWNERS to enable mapping of review processes of larger organizations and organizations that use a monorepo](https://gitlab.com/groups/gitlab-org/-/epics/9774)
    - [Syntax highlighting and basic validation for CODEOWNERS file to help users understand the syntax and avoid mistakes](https://gitlab.com/groups/gitlab-org/-/epics/9818)
- Fix 7 most customer requested bugs in FY24 Q1


#### What we are currently working on
<!-- Scoped to the current month. This section can contain the items that you choose to highlight on the kickoff call. Only link to issues, not Epics.  -->

<!-- 15.10: -->
- Improving Forking experience: [Fetch new upstream contents when fork is behind](https://gitlab.com/gitlab-org/gitlab/-/issues/330243)
- More transparency on what has been committed since a release: [Better display of tags in commits list view](https://gitlab.com/gitlab-org/gitlab/-/issues/18795)
* Adding last set of elements to [branch rule overview](https://gitlab.com/groups/gitlab-org/-/epics/8578) and finally rolling it out. 
  - [Branch rules overview support for "All protected branches"](https://gitlab.com/gitlab-org/gitlab/-/issues/388946)
  - [Show setting for CODEOWNER Approval in Branch Rule detail view](https://gitlab.com/gitlab-org/gitlab/-/issues/389058) 
  - and some more
* Starting to [Enable E2E workflow for setting branch rules - from discovery to changing the rule](https://gitlab.com/groups/gitlab-org/-/epics/9810)
  - [Improve the visibility of branch-based settings](https://gitlab.com/gitlab-org/gitlab/-/issues/358209)

 

<!-- 15.11:
- More transparency on which branches and tags contain a specific commit: [Better display of tags and branches in commit detail view](https://gitlab.com/gitlab-org/gitlab/-/issues/325871) -->

#### What we recently completed
<!-- Lookback limited to 3 months. Link to the relevant issues or release post items. -->
- [Require multiple approvals from CODEOWNERS](https://gitlab.com/gitlab-org/gitlab/-/issues/335451): You can now precisely define for which files, file types, or directories approval has been designated as optional, required approval by 1 user or required approval by multiple users. The latter being the new improvement of CODEOWNERS.

  So far, if you needed to require multiple approvers be it for compliance reasons or other reasons, you could only do so with an approval rule. However, unlike CODEOWNER approvals, approval rules apply to entire branches and cannot be refined to apply to specific parts of your code base. So, the multiple approvals would have also been required for changes that do not need a high level of scrutiny leading to unnecessary reviews. [See documentation.](https://docs.gitlab.com/ee/user/project/code_owners.html#require-multiple-approvals-from-codeowners)
- [More control over your SSH connections with gitlab-sshd](https://gitlab.com/groups/gitlab-org/-/epics/5394): `gitlab-sshd` is a standalone SSH server, written in Go, that provides more insight and control than OpenSSH. It's lightweight and contains minimal external dependencies. If you host a self-managed instance, switching from OpenSSH to `gitlab-sshd` gives you metrics collection, detailed logging, and graceful shutdowns for SSH connections. Unlike OpenSSH, it supports the PROXY protocol and can pass on the original IP address when operated behind a proxy. This enables you to [restrict SSH access by IP address](https://docs.gitlab.com/ee/user/group/access_and_permissions.html#restrict-group-access-by-ip-address) when your instance is operated behind a proxy. 

  GitLab.com has used `gitlab-sshd` since 15.2, and 100% of the SSH traffic passes through `gitlab-sshd`. To learn more, read [this blog post](https://about.gitlab.com/blog/2022/08/17/why-we-have-implemented-our-own-sshd-solution-on-gitlab-sass/). To understand how to enable it refer to [the documentation](https://docs.gitlab.com/ee/administration/operations/gitlab_sshd.html).

  `gitlab-sshd` began as a community contribution from @lorenz. Thank you very much for your contribution! 
- [Sign commits with your SSH key](https://gitlab.com/gitlab-org/gitlab/-/issues/343879): Signing commits just got a lot simpler. [Use SSH keys to sign commits](https://docs.gitlab.com/ee/user/project/repository/ssh_signed_commits/), and provide others with confidence that a Verified commit was authored by you.

  Previous methods for signing commits required a GPG key or an X.509 certificate, neither of which can be used to sign in to GitLab. Adding support for commit signing with SSH keys now makes it possible to reuse your authentication key pair to also sign your commits. If you already authenticate into GitLab with an SSH key, add three lines of code to your local Git configuration and all your future commits will be signed.

  By default, all SSH keys currently in your profile can be used for both authentication and signing commits. To use a key for only one of the purposes, upload a new key.



#### What is Not Planned Right Now
<!--  Often it's just as important to talk about what you're not doing as it is to
discuss what you are. This section should include items that people might hope or think
we are working on as part of the category, but aren't, and it should help them understand why that's the case.
Also, thinking through these items can often help you catch something that you should
in fact do. We should limit this to a few items that are at a high enough level so
someone with not a lot of detailed information about the product can understand -->

The Source Code group is not investing in the following opportunities in the immediate future: 

- [Branch read access controls](https://gitlab.com/gitlab-org/gitlab-ee/issues/720) 

   Limiting which branches a user can read in a Git repository is possible in a basic sense, by only advertising a subset of refs, but it is not possible to guarantee that unreachable objects will not be sent to the client. This means that branch read access controls would be very weak, since they could not prevent exfiltration of data they do not have permission to read. 

- Path-level read access controls 

   From a commit, Git expects all trees and blobs to be reachable. Although Git supports partial clone and spares checkout, which allow data to be excluded from fetch and checkout, Git expects to be able to fetch missing objects on demand. Deliberately excluding objects by path is likely to cause unexpected failures. 



### Best in Class Landscape
<!-- Blanket description consistent across all pages that clarifies what GitLab means when we say "best in class" -->
BIC (Best In Class) is an indicator of forecasted near-term market performance based on a combination of factors, including analyst views, market news, and feedback from the sales and product teams. It is critical that we understand where GitLab appears in the BIC landscape. 

#### Key Capabilities 
<!-- For this product area, these are the capabilities a best-in-class solution should provide -->

This information is maintained on [this internal handbook page](https://internal-handbook.gitlab.io/handbook/product/best-in-class/create/#source-code-management)

#### Roadmap
<!-- Key deliverables we're focusing on to build a BIC solution. List the epics by title and link to the epic in GitLab. Minimize additional description here so that the epics can remain the SSOT. -->

This information is maintained on [this internal handbook page](https://internal-handbook.gitlab.io/handbook/product/best-in-class/create/#source-code-management)

#### Top two Competitive Solutions
<!-- PMs can choose to highlight a primary BIC competitor--or more, if no single clear winner in the category exists; in this section we should indicate: 1. name of competitive product, 2. links to marketing website and documentation, 3. why we view them as the primary BIC competitor -->

This information is maintained on [this internal handbook page](https://internal-handbook.gitlab.io/handbook/product/best-in-class/create/#source-code-management)


### Maturity Plan
<!-- It's important your users know where you're headed next. The maturity plan section captures this by showing what's required to achieve the next level. The
section should follow this format:

This category is currently at the XXXX maturity level, and our next maturity target is YYYY (see our [definitions of maturity levels](https://about.gitlab.com/direction/maturity/)).

- Link to maturity epic if you are using one, otherwise list issues with maturity::YYYY labels) 

(For non-marketing categories this section is optional)  -->

This [category is currently of **Loveable** maturity level](https://gitlab.com/gitlab-org/ux-research/-/issues/941) (see our [definitions of maturity levels](https://about.gitlab.com/direction/maturity/)). 

[We plan to re-assess the maturity level in FY24-Q1](https://gitlab.com/gitlab-org/ux-research/-/issues/2351).

### Target Audience
<!--
List the personas (https://about.gitlab.com/handbook/marketing/strategic-marketing/roles-personas#user-personas) involved in this category.

Look for differences in user's goals or uses that would affect their use of the product. Separate users and customers into different types based on those differences that make a difference.
-->

All GitLab users use the Source Code category. The more intensive users are the following:
1. [Sasha, Software Developer](/handbook/product/personas/#sasha-software-developer) 
2. [Priyanka, Platform Engineer](/handbook/product/personas/#priyanka-platform-engineer)
3. [Delaney, Development Team Lead](/handbook/product/personas/#delaney-development-team-lead)
4. [Cameron, Compliance Manager](/handbook/product/personas/#cameron-compliance-manager)
